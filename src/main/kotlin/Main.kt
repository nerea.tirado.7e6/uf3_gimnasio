import java.io.BufferedReader
import java.nio.file.Path
import kotlin.io.path.readLines
import kotlin.io.path.writeText
import java.io.File
import java.io.FileReader
import kotlin.io.path.createDirectory
import kotlin.io.path.exists
import kotlin.io.path.listDirectoryEntries
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import kotlin.experimental.ExperimentalTypeInference
import kotlin.io.path.Path

fun main() {
    var ruta = Path("./src/main/kotlin")

    do {
        //opcMenu
        println("BIENVENIDO AL MENU DEL PROGRAMA")
        println("1. Modificar usuario")
        println("2. Consultar usuarios")
        println("3. Crear usuarios")
        println("4. Desbloquear/bloquear usuarios")
        println("5. Leer usuarios")
        println("6. Hacer backups")
        println("0. Salir")
        //var opcMenu = readln()
        println("Selecciona que opción del menu quieres ver:")
        var opcSeleccionada = readln().toInt()

        when(opcSeleccionada){
            1 -> modificarUsuario(ruta)
            2 -> mostrarUsuarios(ruta)
            3 -> crearUsuario(ruta)
            4 -> desbloqueado_bloqueado(ruta)
            5 -> importarUsuarios(ruta)
            6 -> hacerBackups(ruta)
            0 -> println("Has seleccionado salir del juego")
        }
    } while (opcSeleccionada != 0)

}

//que tiene que contener ese usuario para ser aceptado
fun leerUsuario(minimo: Int, maximo:Int):Int {
    var usuarioIntroducido = readln().toInt()

    while (usuarioIntroducido < minimo || usuarioIntroducido > maximo){
        println("Tienen que estar entre el $minimo y el $maximo")
        usuarioIntroducido = readln().toInt()
    }
    return usuarioIntroducido
}

//crear usuario para acceder
fun crearUsuario(ruta:Path):String{
    val ultimaIdUsuario = ultimId(ruta.resolve("data/userData.txt").toFile())
    val id = ultimaIdUsuario+1

    println("Introduce tu nombre: ")
    val nombre = readln()
    println("Introduce el numero de telefono: ")
    val numeroTelefono = readln()
    println("Introduce el correo electronico: ")
    var correo = readln()

    val activo = true
    val bloqueado = false

    var datosAñadidos = "$id;$nombre;$numeroTelefono;$correo;$activo;$bloqueado"
    datosEnFichero(ruta,datosAñadidos)
    return datosAñadidos
}


fun modificarUsuario(ruta: Path) {

    fun modificarCamposDelUsuario(datos:List<String>):List<String>{
        val nuevosDatos = mutableListOf<String>()
        for(dato in datos){
            nuevosDatos.add(dato)
        }

        println("Nombre del usuario:")
        nuevosDatos[1] = readln()
        println("Teléfono del usuario:")
        nuevosDatos[2] = readln()
        println("Email del usuario:")
        nuevosDatos[3] = readln()

        return nuevosDatos
    }
    cambiarUsuario(ruta,::modificarCamposDelUsuario)
}

fun desbloqueado_bloqueado(ruta:Path) {

    fun cambiarDatos(datos:List<String>):List<String>{
        val nuevosDatos = mutableListOf<String>()
        for(dato in datos){
            nuevosDatos.add(dato)
        }

        val bloq = !datos[5].toBoolean()
        nuevosDatos[5] = bloq.toString()
        if(bloq){
            println("El usuario ${datos[0]} esta bloqueado")
        }else{
            println("L'usuari ${datos[0]} ha sido desbloqueado")
        }
        return nuevosDatos
    }
    cambiarUsuario(ruta,::cambiarDatos)
}

fun cambiarUsuario(ruta:Path,lambda:(List<String>)->List<String>){
    val fichero = ruta.resolve("data/userData.txt")
    val usuarios = fichero.readLines().toMutableList()

    println("Que usuario quieres modificar, introduce su ID:")
    val usuarioModificarID = readln().toInt()

    var existe = false
    for (usuario in usuarios.indices){
        val campos = usuarios[usuario].split(";")
        //println(usuarios)
        if (campos[0].toInt()==usuarioModificarID){
            existe = true
            val nuevosDatos = lambda(campos)
            usuarios[usuario]= nuevosDatos.joinToString(";")
            break
        }
    }
    if(existe==false){
        println("No se ha encontrado al usuario con la ID $usuarioModificarID")
        return
    }
    val newContent = usuarios.joinToString("\n")
    fichero.writeText(newContent)
}

//añadir usuario
fun datosEnFichero(ruta: Path,datos:String){
    //comprobar que la ultima linea no es linea vacia
    val file = ruta.resolve("data/userData.txt").toFile()
    file.appendText("\n"+datos)
}
//muestra usuarios
fun mostrarUsuarios(ruta: Path){
    var usu = leerFichero(ruta.resolve("data/userData.txt"))
    println(usu)
}

//IMPORTS
fun importarUsuarios(ruta:Path){
    //buscar
    var importar = ruta.resolve("import/")

    val numeroFichero = contarFicheros(importar)
    if (numeroFichero>0){
        println("Importando archivos")

        //ver fichero
        var userData = ruta.resolve("data/userData.txt").toFile()
        if (!userData.exists()){
            userData.createNewFile()
        }

        var ficheros = listaFicheros(importar)
        for (fichero in ficheros){
            importarAntiguoFichero(userData, fichero)

            eliminar(fichero)
        }
    }
}
//recibir fichero antiguo
fun importarAntiguoFichero(nuevoFichero: File, antiguoFichero:Path) {
    var antiguoContenido = leerFichero(antiguoFichero)
    var antiguoUsuario = antiguoContenido.split(";")

    var usuarios = mutableListOf<String>()

    var ultimaId = ultimId(nuevoFichero)
    for (usuario in antiguoUsuario) {
        //actualizamos usuarios
        var nuevoUsuario = actualizarUsuario(usuario, usuarios.size+ultimaId+1)

        //sin añadir los bloqueados
        if (nuevoUsuario[4].toBoolean() || !nuevoUsuario[5].toBoolean()) {
            usuarios.add(nuevoUsuario.joinToString(";"))
        }
    }
    var nuevoContenido = usuarios.joinToString("\n")
    nuevoFichero.appendText(nuevoContenido)
}

fun ultimId(file: File):Int {
    val input = BufferedReader(FileReader(file))
    var resultado = "0"

    while (input.ready()){
        val linea = input.readLine()
        if (linea != "") {
            resultado=linea
        }
    }
    val id = resultado.split(';')[0]

    return id.toInt()
}

fun actualizarUsuario(antiguo:String, id:Int):List<String>{
    var campos = antiguo.split(",")
    var nuevosCampos = mutableListOf<String>()
    for (campo in campos){
        nuevosCampos.add(campo)
    }
    nuevosCampos[0] = id.toString()
    return nuevosCampos
}

//FUNCIONALIDAD
//contamos si hay ficheros en la carpeta
fun contarFicheros(path: Path):Int{
    if (path.exists()){
        var ficheros = path.listDirectoryEntries()
        return ficheros.size
    }
    return 0
}
//lista de ficheros que hay ene sa carpeta
fun listaFicheros(path: Path):List<Path>{
    return path.listDirectoryEntries()
}
fun eliminar(path: Path):Boolean{
    var fichero = path.toFile()
    if (fichero.exists()){
        var aceptado = fichero.delete()
        return aceptado
    }
    return false
}
//contenido en String
fun leerFichero(path: Path):String{
    var fichero = path.toFile()
    var resultado = ""
    fichero.forEachLine {
        resultado+=it+"\n"
    }
    return resultado
}
fun crearFicheros(file: File, contenido:String=""){
    if (!file.exists()){
        var aceptado = file.createNewFile()
        if (aceptado){
            file.writeText(contenido)
        }
    }
}
//si no existe lo crea
fun crearFicheronoExistente(path:String):File{
    val file = File(path)
    if(!file.exists()){
        file.createNewFile()
    }
    return file
}
//mirar si la carpeta existe
fun carpetaExistente(ruta: Path, nombreCarpeta:String):Path{
    var carpeta = ruta.resolve(nombreCarpeta)
    if (!carpeta.exists()){
        carpeta.createDirectory()
    }
    return carpeta
}

//BACKUPS
fun hacerBackups(ruta: Path){
    var contenido = leerFichero(ruta.resolve("data/userData.txt"))

    //nombre fichero
    var fechaActual = LocalDateTime.now()
    var modificarfecha = DateTimeFormatter.ofPattern("yyyy-MM-dd")
    var fechaMod = fechaActual.format(modificarfecha)
    var nombreFichero = fechaMod + "userData.txt"

    var path = ruta.resolve("backups/")
    var fichero = path.resolve(nombreFichero).toFile()
    //si no existe lo crea
    if (fichero.exists()){
        fichero.writeText(contenido)
    } else {
        crearFicheros(fichero, contenido)
    }
    println("El backups se ha hecho con exito")
}